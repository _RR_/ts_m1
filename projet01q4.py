#!/usr/bin/python3

import sys
from signal import Signal
import workflow as wf
from midi import Midi
from freq import FrequencyConverter

if len(sys.argv) != 2:
    print("Use " + sys.argv[0] + " [sound_file]")
    print("to analyse and hopefully transcribe this sound file")
    sys.exit(1)

s0 = Signal()
print("Loading file '" + sys.argv[1] + "' ...")
s0.load_wav(sys.argv[1])

# Computing envelope of the squared signal
s1, s2 = wf.envelope(s0, 50, 400)
# Computing the differentiate of the envelope
s3 = wf.derive(s2, 10, 15)
# Thresholding to find the notes positions
index, s4 = wf.threshold(s3, 0.35)
# Computing the frequencies of the notes (in the order)
freqs = wf.frequencies(s0, index, min_smoothing=True)

# Computing time spaces between notes
time_spaces = [1]
for i in range(len(index)-1):
    time_spaces.append(1+index[i+1]/s0.rate)
# A midi tick is 0.0005 seconds of time

print("Creating midi from notes...")
index.append(len(s4.data))

fconv = FrequencyConverter()
midi = Midi()
for i in range(len(freqs)):
    if isinstance(freqs[i], type([])):
        print(freqs[i])
        for f in freqs[i]:
            nf = int(fconv.frequency_to_midi_number(f))
            midi.add_note_midi(nf,
                               time_spaces[i]*2,
                               1)
    else:
        f = int(fconv.frequency_to_midi_number(freqs[i]))
        midi.add_note_midi(f,
                           time_spaces[i]*2,
                           1)
midi.to_file("test.mid")
