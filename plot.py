#!/usr/bin/python3
from matplotlib import pyplot as plt


class SignalPlot:
    def __init__(self, title):
        self.fig, self.ax = plt.subplots(1, 1)
        self.ax.set_title(title)
        self.ax.set_xlabel('Time (s)')
        self.ax.set_ylabel('Value')

    def plot(self, signal, param_dict={}):
        """ Plot a signal with given parameter for the plot """
        time = [float(i)/float(signal.rate) for i in range(len(signal.data))]
        self.ax.plot(time, signal.data, **param_dict)
        self.ax.grid()

    def show_all(self):
        """ Show all the plotted signals """
        plt.show()

    def plot_attacks(self, signal, x_array_index, param_dict={}):
        """ Plot a signal and a given list of attacks """
        time = [float(i)/float(signal.rate) for i in range(len(signal.data))]
        self.ax.plot(time, signal.data, **param_dict)
        for i in x_array_index:
            self.ax.axvline(x=time[i], linewidth=1, color='red')
        self.ax.grid()

    def save_plot(self, name, param_dict={}):
        self.ax.savefig(name, **param_dict)
