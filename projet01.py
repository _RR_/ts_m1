#!/usr/bin/python3

import sys
from signal import Signal
import workflow as wf

if len(sys.argv) != 2:
    print("Use " + sys.argv[0] + " [sound_file]")
    print("to analyse and hopefully transcribe this sound file")
    sys.exit(1)

s0 = Signal()
print("Loading file '" + sys.argv[1] + "' ...")
s0.load_wav(sys.argv[1])

s1 = wf.envelope(s0, 20, 3)
