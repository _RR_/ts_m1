#!/usr/bin/python3

from midiutil import MIDIFile


class Midi:
    def __init__(self, tempo=120):
        self.tempo = tempo  # In BPM
        self.track = 0
        self.time = 0    # In beats
        self.duration = 1    # In beats
        self.volume = 100  # 0-127, as per the MIDI standard
        self.midi = MIDIFile(1, removeDuplicates=False,
                             deinterleave=False)  # One track
        self.midi.addTempo(self.track, self.time, self.tempo)

    def to_file(self, file_path):
        with open(file_path, "wb") as output_file:
            self.midi.writeFile(output_file)

    def add_note_midi(self, pitch, time, duration, channel=0, volume=100):
        self.midi.addNote(self.track, channel, pitch,
                          time, duration, volume)
