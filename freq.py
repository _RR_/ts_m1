#!/usr/bin/python3

import numpy as np


class FrequencyConverter:
    def __init__(self, f_a4=440.0):
        self.f_a4 = float(f_a4)

    def piano_key_number_to_frequency(self, n):
        return np.power(2, (n - 49.0) / 12.0) * self.f_a4

    def frequency_to_piano_key_number(self, freq, round_result=True):
        if round_result:
            return np.round(12.0 * np.log2(freq / self.f_a4) + 49.0)
        else:
            return 12.0 * np.log2(freq / self.f_a4) + 49.0

    def midi_number_to_frequency(self, n):
        return self.f_a4 * np.power(2, (n - 69.0) / 12.0)

    def frequency_to_midi_number(self, freq, round_result=True):
        if round_result:
            return np.round(12.0 * np.log2(freq / self.f_a4) + 69.0)
        else:
            return 12.0 * np.log2(freq / self.f_a4) + 69.0
