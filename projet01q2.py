#!/usr/bin/python3

import sys
from signal import Signal
import workflow as wf
from plot import SignalPlot
import numpy as np
from scipy.signal import periodogram as speriodogram
if len(sys.argv) != 2:
    print("Use " + sys.argv[0] + " [sound_file]")
    print("to analyse and hopefully transcribe this sound file")
    sys.exit(1)

s0 = Signal()
print("Loading file '" + sys.argv[1] + "' ...")
s0.load_wav(sys.argv[1])

s1, s2 = wf.envelope(s0, 50, 400)
s3 = wf.derive(s2, 10, 15)
index, s4 = wf.threshold(s3, 0.35)
plt = SignalPlot("Attacks detected")
freqs = wf.frequencies(s0, index)
print(freqs)
