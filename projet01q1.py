#!/usr/bin/python3

import sys
from signal import Signal
import workflow as wf
from plot import SignalPlot
import numpy as np

if len(sys.argv) != 2:
    print("Use " + sys.argv[0] + " [sound_file]")
    print("to analyse and hopefully transcribe this sound file")
    sys.exit(1)

s0 = Signal()
print("Loading file '" + sys.argv[1] + "' ...")
s0.load_wav(sys.argv[1])

s1, s2 = wf.envelope(s0, 50, 300)
s3 = wf.derive(s2, 10, 15)
index, s4 = wf.threshold(s3, 0.35)

plt = SignalPlot("Original Signal")
plt.plot(s0)
plt = SignalPlot("Squared Signal")
plt.plot(s1)
plt = SignalPlot("Low Frequencies only (filtered) Squared Signal")
plt.plot(s2)
plt = SignalPlot("Differentiate Signal")
plt.plot(s3)
plt = SignalPlot("Thresholded Signal")
plt.plot(s4)
plt = SignalPlot("Attacks detected")
plt.plot_attacks(s0, index)
plt.show_all()
