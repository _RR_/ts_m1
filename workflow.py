#!/usr/bin/python3


def envelope(s0, f_cut=20, filter_order=3, window='hamming'):
    """ Compute the envelope of the given signal s0 using
        double alternate rectification and a lowpass filter
        with the given parameters """
    print("Computing signal envelope...")
    print("\tApplying double alternate rectification")
    s1 = s0.double_alternate_rectification()
    print("\tApplying low pass filter")
    s2 = s1.low_pass(f_cut, filter_order)
    return (s1, s2)


def derive(s0, filter_order, cut_off_frequency):
    """ Derive signal s0 """
    print("Computing derivative")
    print("\tApplying derivative filter")
    return s0.diff_filter(cut_off_frequency, filter_order)


def threshold(s0, max_duration=0.2):
    """ Threshold signal """
    print("Computing threshold")
    indexs, s1 = s0.threshold(max_duration)
    index = [i[0] for i in indexs]
    return index, s1


def frequencies(s0, index_attacks, method="auto", window_size=512,
                method_freq_tot="hpsm", repetition=4, min_smoothing=False):
    """ Computing frequencies with vote or without """
    print("Computing frequencies")
    if method == "auto":
        frequencies = s0.notes_frequencies_total(index_attacks,
                                                 method_freq_tot,
                                                 repetition,
                                                 min_smoothing)
    else:
        frequencies = s0.notes_frequencies_windowed(index_attacks, window_size)
    return frequencies
