#!/usr/bin/python3

import numpy as np
# Strange import pbs with venv
# import scipy as sp
from scipy.io.wavfile import read as wavread
from scipy.signal import firwin as spfirwin
from scipy.signal import lfilter as splfilter
# from plot import SignalPlot
# from scipy.signal import remez


class Signal:
    def __init__(self, signal=None):
        if signal is None:
            self.init = False
        else:
            if type(signal) == type(self):
                if(signal.init):
                    self.init = True
                    self.rate = signal.rate
                    self.data = np.copy(signal.data)
                else:
                    raise Exception("Building from an uninitialized signal")
            else:
                raise Exception("Invalid object to construct from, \
not a signal")

    def load_wav(self, path, normalize=True):
        """ Load Sound file """
        self.rate, self.data = wavread(path, mmap=False)
        if(normalize):
            self.data = normalized(self.data)
        self.init = True

    def from_data_with_same_rate(self, data):
        """ Create a new signal object with the same rate """
        assert self.init
        s = Signal()
        s.rate = self.rate
        s.data = np.copy(data)
        s.init = True
        return s

    def low_pass(self, cut_off_freq, filter_order=3, window='hamming'):
        """ Apply a low pass filter to a signal """
        assert self.init
        h = spfirwin(numtaps=filter_order,
                     cutoff=cut_off_freq, window=window, fs=self.rate,
                     pass_zero='lowpass')
        out_data = splfilter(h, 1.0, self.data)
        return self.from_data_with_same_rate(out_data)

    def diff_filter(self, cut_off_freq, filter_order=3, window="hamming"):
        """ Apply a differentiator filter to a signal """
        assert self.init
        # h = spfirwin(numtaps=filter_order+1, cutoff=cut_off_freq,
        #             window=window, pass_zero='highpass', fs=self.rate)
        # out_data = splfilter(h, 1.0, self.data)
        out_data2 = splfilter([1.0, -1.0], 1.0 / self.rate, self.data)
        h = spfirwin(numtaps=30, cutoff=2, fs=self.rate, pass_zero='lowpass')
        out_data2 = splfilter(h, 1.0, out_data2)
        return self.from_data_with_same_rate(out_data2)

    def double_alternate_rectification(self):
        """ Compute the double alternate rectification to a signal """
        assert self.init
        return self.from_data_with_same_rate(np.power(np.abs(self.data), 2))

    def find_nb_notes(self, max_duration):
        """ Find the number of notes with information in the
            differentiated signal """
        assert self.init
        max_value = max(self.data)
        nb_notes = 0
        i = 0
        i_above = - max_duration * self.rate
        above = False
        while i < len(self.data):
            if self.data[i] > max_value / 4 \
               and (i-i_above)/self.rate > max_duration:
                print((i-i_above)/self.rate, max_duration)
                i_above = i
                if not(above):
                    above = True
                    nb_notes += 1
            else:
                above = False
            i += 1
        print(nb_notes)
        return nb_notes

    def threshold(self, max_duration=0.35):
        """ Find the index of a given number of notes that last less than a
            given duration. Returns the signal that only contains notes and
            a list of index where the notes are played """
        # Find the threshold delimiting a given number of notes
        assert self.init
        # Initialize the dichotomic search
        nb_notes = self.find_nb_notes(max_duration)
        maxi, mini = max(self.data), 0.0
        notes_detected = 0
        threshold = (maxi + mini) / 2
        # We change the threshold until we have the desired number of notes
        while notes_detected != nb_notes:
            notes_detected = 0
            # Index above the threshold
            peaks_index = []
            # Index above the threshold grouped by note
            peaks_index_grouped = []
            # We fill the peaks_index list
            for i, v in enumerate(self.data):
                if v > threshold:
                    peaks_index.append(i)
            last_index = peaks_index[0]
            group = [last_index]
            # Group the peaks index if they are closer than the max_duration
            # The number of group is the number of notes detected
            for i, v in enumerate(peaks_index):
                if (v - last_index)/self.rate > max_duration:
                    notes_detected += 1
                    last_index = v
                    peaks_index_grouped.append(group)
                    group = [v]
                else:
                    group.append(v)
            peaks_index_grouped.append(group)
            notes_detected += 1
            # Change the threshold following the dichotomic method
            if notes_detected > nb_notes:
                mini, threshold = threshold, (maxi + threshold)/2
            else:
                maxi, threshold = threshold, (mini + threshold)/2
        # We build the signal with zero when there isn't any note
        s = []
        empty_start = 0
        for g in peaks_index_grouped:
            c_start, c_end = g[0], g[-1]
            s = np.concatenate((s,
                                np.array([0] * (c_start - empty_start)),
                                self.data[c_start:c_end]))
            empty_start = c_end
        s = np.concatenate((s,
                            np.array([0]*(len(self.data) - empty_start))))
        peaks_index_pair = [(g[0], g[-1]) for g in peaks_index_grouped]
        return peaks_index_pair, self.from_data_with_same_rate(np.array(s))

    def biaised_autocorrelation(self):
        """ Compute biased autocsovariance """
        N = len(self.data)

        def rx(k):
            delk_lst = np.array(self.data[:N-k])
            delk_fst = np.array(self.data[k:])
            return np.sum(delk_fst*delk_lst)
        cov = []
        for k in range(N):
            cov.append((1.0 / N) * rx(k))
        return np.array(cov)

    def periodogram(self):
        """ Compute the periodogram of the signal"""
        assert self.init
        out_data = list(abs(np.fft.fft(self.biaised_autocorrelation())))
        out_data = out_data[:len(out_data)//2]
        freqs = np.fft.fftfreq(len(out_data), 1.0 / self.rate)
        freqs = freqs[:len(freqs)//2]
        return self.from_data_with_same_rate(out_data), freqs

    def frequence(self):
        """ Give the frequence of the signal with the periodogram """
        assert self.init
        periodogram, freqs = self.periodogram()
        maxi = max(periodogram.data)
        f = freqs[list(periodogram.data).index(maxi)]
        fond = 1.0

        def freq_index(f, freqs):
            for i, f_v in enumerate(freqs):
                if f <= f_v:
                    return i
            return len(freqs) - 1
        while periodogram.data[freq_index(f/fond, freqs)] > maxi/10:
            fond += 1
        return f/fond

    def frequence_product(self, fft_array=None, array_rate=None,
                          repetitions=4):
        assert self.init
        if fft_array is not None:
            N = 2*len(fft_array)
            fft_prod = abs(fft_array)
            rate = array_rate
        else:
            N = len(self.data)
            fft_prod = abs(np.fft.fft(self.data))
            rate = self.rate
            fft_prod = fft_prod[: len(fft_prod) // 2]
        freqs = np.fft.fftfreq(N, 1.0 / rate)
        freqs = freqs[: len(freqs)//2]
        for i in range(1, repetitions):
            sub_sampled = [fft_prod[j] for j in range(0, len(fft_prod), i)]
            fft = list(sub_sampled) + [0] * (N // 2 - len(sub_sampled))
            fft_prod *= np.array(fft)
        index_max = list(fft_prod).index(max(fft_prod))
        f = freqs[index_max]
        for i in range(0, index_max):
            if fft_prod[i] > 0.5 * max(fft_prod):
                return freqs[i]
        return f

    def attacks_signals(self, index_attacks):
        """ Signal splitted between attacks """
        signals = []
        for i in range(len(index_attacks)):
            start = index_attacks[i]
            if i < len(index_attacks)-1:
                end = index_attacks[i+1]
            else:
                end = len(self.data)
            signals.append(self.from_data_with_same_rate(self.data[start:end]))
        return signals

    def notes_frequencies_total(self, index_attacks, method=None, repetition=4,
                                min_smoothing=False):
        """ Compute the frequencies of the notes in the signal """
        attacks_signals = self.attacks_signals(index_attacks)
        frequencies = []
        for signal in attacks_signals:
            if method == "hpsm":
                f = signal.multiple_frequency_simple_iterate(repetition,
                                                             min_smoothing)
            else:
                f = signal.frequence_product()
            frequencies.append(f)
        return frequencies

    def notes_frequencies_windowed(self, index_attacks, window_size=512):
        """ Compute the frequencies of the notes in the signal with a vote """
        attacks_signals = self.attacks_signals(index_attacks)
        frequencies = []
        for signal in attacks_signals:
            frequencies_seen = []
            seen_number = []
            split = [i for i in range(0, len(signal.data), window_size)]
            windows = signal.attacks_signals(split)
            del windows[-1]
            for window in windows:
                f = window.frequence()
                if f in frequencies_seen:
                    seen_number[frequencies_seen.index(f)] += 1
                else:
                    frequencies_seen.append(f)
                    seen_number.append(1)
            f = frequencies_seen[seen_number.index(max(seen_number))]
            frequencies.append(f)
        return frequencies

    def multiple_frequency_simple_iterate(self, repetitions=4,
                                          min_method=False):
        """  Compute multiple note frequency using
             a simple substractive method an iterating it """
        freqs = np.fft.fftfreq(len(self.data), 1.0 / self.rate)
        self.freqs = freqs[:len(freqs)//2]
        data = abs(np.copy(np.fft.fft(self.data)))
        data = data[:len(data)//2]
        data[0] = 0
        threshold = np.max(data)/3
        pic_width = 16
        window_width = 32
        number_freq = 4
        flist = []
        # while there is still potential frequencies
        while np.max(data) > threshold and len(flist) < number_freq:
            # compute proeminent frequency
            f = self.frequence_product(data, self.rate, repetitions)
            i = 1
            max_f = np.max(self.freqs)
            temp_freq = f * i
            # remove f and its harmonics from the spectrogram
            # plt = SignalPlot("FFT signal")
            # plt.plot(self.from_data_with_same_rate(data))
            while temp_freq < max_f:
                # identify where f pic is in terms of indices
                center_pic = None
                for j in range(len(self.freqs)):
                    if self.freqs[j] >= temp_freq:
                        center_pic = j
                        break
                if center_pic is None:
                    print("You should not pass here")
                    break
                # plt.ax.axvline(x=j/len(self.data), linewidth=1, color='red')
                # plt.show_all()
                # center pic indice is init
                left_pic = max(center_pic - int(pic_width/2), 0)
                right_pic = min(center_pic + int(round(pic_width/2)),
                                len(data))
                farleft_pic = max(center_pic - int(window_width/2), 0)
                farright_pic = min(center_pic + int(round(window_width/2)),
                                   len(data))
                if min_method:
                    # Apply min smoothing window method
                    tmp = 0
                    counter = 0
                    for ki in range(farleft_pic, left_pic):
                        tmp += data[ki]
                        counter += 1
                    for ki in range(right_pic, farright_pic):
                        tmp += data[ki]
                        counter += 1
                    min_value = tmp/counter
                else:
                    # compute min in the large window (far...)
                    min_value = np.min(data[farleft_pic:farright_pic])
                data2 = []
                # remplace values in the small window by min_value
                for k in range(len(data)):
                    if k >= left_pic and k <= right_pic:
                        # do not up the FFT (min)
                        data2.append(min(min_value, data[k]))
                    else:
                        data2.append(data[k])
                # save data
                data = np.array(data2)
                # go next harmonic
                i += 1
                temp_freq = f * i
            # Done removing harmonic
            # append freq to flist and here we go again
            flist.append(f)

        return flist


# Thanks: https://stackoverflow.com/questions \
# /21030391/how-to-normalize-an-array-in-numpy#21031303
def normalized(a, axis=-1, order=2):
    """ Normalize a signal """
    l2 = np.atleast_1d(np.linalg.norm(a, order))
    l2[l2 == 0] = 1
    return a / l2
