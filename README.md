# TS Transcription

Install requirements using `pip install -r requirements.txt`

Then you can use `python projet01q4.py <path_to_mono_wav_file>` to create a `test.mid` file containing
the transcription of the sound file.

The various other executable scripts are for ploting purposes only.